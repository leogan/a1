
# ******************************************************************************
# * Copyright (C) 2020 by Jonathan Appavoo, Boston University
# *
# * Permission is hereby granted, free of charge, to any person obtaining a copy
# * of this software and associated documentation files (the "Software"), to deal
# * in the Software without restriction, including without limitation the rights
# * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# * copies of the Software, and to permit persons to whom the Software is
# * furnished to do so, subject to the following conditions:
# *
# * The above copyright notice and this permission notice shall be included in
# * all copies or substantial portions of the Software.
# *
# * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# * THE SOFTWARE.
# *****************************************************************************/

define setRealMode
  set tdesc filename kex.i386.xml
  set arch i8086
end 
document setRealMode
  This command loads the protocol definition for 32 bit register layout 
  when kex is executing in 32 or 16 bit mode.   It also sets the 
  architecture to i8086 for intel 16 bit dissasembly
end

define setProtectedMode
  set tdesc filename kex.i386.xml
  set arch i386
end
document setProtectedMode
  This command loads the protocol definition for 32 bit register layout 
  when kex is executing in 32 or 16 bit mode.   It also sets the 
  architecture to i386  for 32 bit intel disassembly.
end

define setLongMode
  set tdesc filename kex.x86-64.xml
  set arch i386:x86-64	
end
document setLongMode
  This command loads the protocol definition for 64 bit register layout 
  when kex is executing in 32 or 16 bit mode.   It also sets the 
  architecture to i386  for 32 bit intel disassembly.
end


define ui
  tui disable
  tui enable
  layout asm
  layout reg
  winh reg 20
  focus asm
end
document ui
  Switch to text windowed ui with a layout that is convient for assemble and register 
  exploration
end


define startKex
  target remote | ssh -o "StrictHostKeyChecking=no" -T -i 210.key gigan.bu.edu
end
document startKex
  Start a fresh machine.  You may only run a single machine at a time.
  When you quit gdb or run the killKex command you will destroy the 
  machine and all its memory and register state will be gone.
end

define killKex
  shell ssh -o "StrictHostKeyChecking=no" -i 210.key gigan.bu.edu kill
end
document killKex
 If you have lost your machine and can't figure out where it is and 
 the system  refuses to let you start a new one use the kill command
 to forcibly destroy your existing machine or cleanup any ghosts of it
end

define stopKex
  shell ssh -o "StrictHostKeyChecking=no" -i 210.key gigan.bu.edu stop
end
document stopKex
  Trys to stop your machine if it is hung in a loop
end

define hook-interrupt
   shell stopKex
end

define setup
  setLongMode
  set lang asm
  break *0x1000
end

define xrip
  x/16bx $rip
end
document xrip
  examine 16 bytes at where the rip is point
end

define bxrip
  x/16bt $rip
end
document xrip
  examine 16 bytes at where the rip is point
end

define savegprs 
  dont-repeat
  if $argc != 1
    printf "You must specify a file to save registers to\n"
    return
  end
  dump binary value $arg0 { $rax,$rbx,$rcx,$rdx,$rsi,$rdi,$rbp,$rsp,$r8,$r9,$r10,$r11,$r12,$r13,$r14,$r15,$rip}
  append binary value $arg0 { $eflags }
end
document savegprs
  savegprs <file>
  Saves the current contents of the gprs to the specified file
end

define savemem 
  dont-repeat
  if $argc != 3
    printf "You must specify a file, start address, and end address\n"
    return
  end
  dump binary memory $arg0 $arg1 $arg2
end
document savemem
  savemem <file> <startaddr> <endaddr>
  save the bytes of memory starting at startaddr to endaddr to the specified file
end

define loadmem
  dont-repeat
  if $argc < 2
    printf "You must specify a file to load and start address"
    return
  end
  if $argc == 2
     restore $arg0 binary $arg1
  end
  if $argc == 3
     restore $arg0 binary $arg1 $arg2 
  end
  if $argc == 4
     restore $arg0 binary $arg1 $arg2 $arg3
  end
end
document loadmem
  loadmem <file> <addr> [start file offset] [end file offset]
  loads the bytes of the specified file to memory at addr
  if you specific a start file offset and or and end file offset you can
  load only parts of the file to memory
end

define gprs
  printf "rax:%u(0x%llx) rbx:%u(0x%llx) rcx:%u(0x%llx) rdx:%u(0x%llx)\n", $rax,$rax,$rbx,$rbx,$rcx,$rcx,$rdx,$rdx
  printf "rsi:%u(0x%llx) rdi:%u(0x%llx) rbp:%u(0x%llx) rsp:%u(0x%llx)\n", $rsi,$rsi,$rdi,$rdi,$rbp,$rbp,$rsp,$rsp
  printf " r8:%u(0x%llx)  r9:%u(0x%llx) r10:%u(0x%llx) r11:%u(0x%llx)\n", $r8,$r8,$r9,$r9,$r10,$r10,$r11,$r11
  printf "r12:%u(0x%llx) r13:%u(0x%llx) r14:%u(0x%llx) r15:%u(0x%llx)\n", $r12,$r12,$r13,$r13,$r14,$r14,$r15,$r15
  printf "rip:%u(0x%llx) eflags:%u(0x%llx)\n", $rip,$rip,$eflags,$eflags
end
document gprs
  print the contents of the general purpose registers that is less complicated than what
  info reg will display
end

define bgprs
  printf "Binary General Purpose Register Dump:\n"
  printf "rax: %llx ", $rax
  p/t $rax
  printf "rbx: %llx ", $rbx
  p/t $rbx 
  printf "rcx: %llx ", $rcx
  p/t $rcx
  printf "rdx: %llx ", $rdx
  p/t $rdx
  printf "rsi: %llx ", $rsi
  p/t $rsi
  printf "rdi: %llx ", $rdi
  p/t $rdi
  printf "rbp: %llx ", $rbp
  p/t $rbp
  printf "rsp: %llx ", $rsp
  p/t $rsp
  printf " r8: %llx ", $r8
  p/t $r8
  printf " r9: %llx ", $r9
  p/t $r9
  printf "r10: %llx ", $r10
  p/t $r10
  printf "r11: %llx ", $r11
  p/t $r11
  printf "r12: %llx ", $r12
  p/t $r12
  printf "r13: %llx ", $r13
  p/t $r13
  printf "r14: %llx ", $r14
  p/t $r14
  printf "r15: %llx ", $r15
  p/t $r15
  printf "rip: %llx ", $rip
  p/t $rip
  printf "eflags: %x ", $eflags 
  p/t $eflags
end
document bgprs
  binary dump of general purpose registers
end

define wqw
set ((unsigned long long *)$arg0)[0] = $arg1
end
document wqw 
  wqw <address> <quad word value>
  wqw writes the specified quad word value (8 byte value) to the address specified
end


define wdw 
set ((unsigned int *)$arg0)[0] = $arg1
end
document wdw 
  wdw <address> <double word value>
  wdw writes the specified double word value (4 byte value) to the address specified
end

define ww
set ((unsigned short *)$arg0)[0] = $arg1
end
document wdw 
  ww <address> <word value>
  ww writes the specified word value (2 byte value) to the address specified
end

define wb
set ((unsigned char *)$arg0)[0] = $arg1
end
document  wb
  wb <address> <byte value>
  wb writes the specified byte value to the memory location specified
  eg. wb 0 0b1010000  
  would set the byte at memory address 0 to the 8bit value of 10100000
  the parameters can be expressed in any of the base notation that gdb supports
  so in this case the value could also equivalently be written as
   in hexidecimal
   wb 0 0x90 
   in decimal
   wb 0 144
  the addess can also be in any supported base notation
end


