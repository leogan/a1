# Please write your solution to q3 in this file.
#
# gdb functions are just a way of defining 
# a set of gdb commands (that you would normally give at the gdb prompt)
# as a sequence to be run when you give the name of the function
# eg.
#
# define myPrintRax
#   p /x $rax
# end
#
#
# The goal is that after q3 is run, the memory at address 0x1000
# be set such that the output of x/16x 0x1000 is as follows
#
# 0x1000:	0xf3	0x48	0x0f	0xb8	0xd8	0x48	0x89	0x1c
# 0x1008:	0x25	0x00	0x30	0x00	0x00	0x00	0x00	0x00
#
# and the output of x/2i is as follows
#
# 0x1000:	popcnt %rax,%rbx
# 0x1005:	mov    %rbx,0x3000
#
# Moreover, the output of x/8x 0x3000 should show you
# that memory location 0x3000 now has the value identical to the 
# value of register rbx.

define q3
# the gdb commands you want run for q3 should be placed  in this gdb command
wb 0x1000 0xf3
wb 0x1001 0x48
wb 0x1002 0x0f
wb 0x1003 0xb8
wb 0x1004 0xd8
set $rsi = 0x1000
stepi
wb 0x1005 0x48
wb 0x1006 0x89
wb 0x1007 0x1c
wb 0x1008 0x25
wb 0x100A 0x30
set $rsi = 0x1005
stepi

end
