# Please write your solution to q2 in this file.
#
# gdb functions are just a way of defining 
# a set of gdb commands (that you would normally give at the gdb prompt)
# as a sequence to be run when you give the name of the function
# eg.
#
# define myPrintRax
#   p /x $rax
# end
#
#
# The goal is that after q2 is run, the memory at address 0x1000
# be set such that the output of x/8x 0x1000 is as follows
#
# 0x1000:	0xf3	0x48	0x0f	0xb8	0xd8	0x00	0x00	0x00
#
# and the output of x/1i is as follows
#
# 0x1000:	popcnt %rax,%rbx

define q2
# the gdb commands you want run for q2 should be placed  in this gdb command
wb 0x1000 0xf3
wb 0x1001 0x48
wb 0x1002 0x0f
wb 0x1003 0xb8
wb 0x1004 0xd8
set $rip= 0x1000
stepi

end
