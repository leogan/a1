# Please write your solution to q1 in this file.
#
# gdb functions are just a way of defining 
# a set of gdb commands (that you would normally give at the gdb prompt)
# as a sequence to be run when you give the name of the function
# eg.
#
# define myPrintRax
#   p /x $rax
# end
#
#
# The goal is that after q1 is run, the gprs will be set such that the 
# output of the bgprs command will look as follows.
#
# Binary General Purpose Register Dump:
# rax: 1 $91 = 1
# rbx: 2 $92 = 10
# rcx: c $93 = 1100
# rdx: 61 $94 = 1100001
# rsi: 0 $95 = 0
# rdi: 0 $96 = 0
# rbp: 0 $97 = 0
# rsp: ffffa000 $98 = 11111111111111111010000000000000
# r8: 62 $99 = 1100010
# r9: 0 $100 = 0
# r10: 0 $101 = 0
# r11: 0 $102 = 0
# r12: 0 $103 = 0
# r13: 0 $104 = 0
# r14: 0 $105 = 0
# r15: 0 $106 = 0
# rip: 1000 $107 = 1000000000000
# eflags: 86 $108 = 10000110
#
# NOTE: If you are running your q1 function
# after starting gdb then you will automatically have 
# rbp, rsp, rip and eflags set correctly.

define q1
# the gdb commands you want run for q1 should be placed  in this gdb command

set $rax = 0b00000001
set $rbx = 0b00000010
set $rcx = 0b00001100
set $rdx = 0b01100001
set $r8 = 0b01100010
end