set pagination off
source kex.gdb

echo LOADING: q3.gdb\n
source q3.gdb

killKex

setup
startKex
continue

delete 1

echo RUNNING: q3 from q3.gdb\n
q3

echo TEST1: TESTING CODE AT 0x1000\n
set $rax = 0
set $rip = 0x1000
si
si
dump binary value q3.resbin { $rbx,  *((unsigned long long *)0x3000) }


echo RUNNING: q3 from q3.gdb\n
q3

echo TEST2: TESTING CODE AT 0x1000\n
set $rax = -1 
set $rip = 0x1000
si
si
append binary value q3.resbin { $rbx, *((unsigned long long *)0x3000) }

echo RUNNING: q3 from q3.gdb\n
q3
echo TEST3: TESTING CODE AT 0x1000\n
set $rax = 0xaaaaaaaaaaaaaaaa
set $rip = 0x1000
si
si
append binary value q3.resbin { $rbx, *((unsigned long long *)0x3000) }


quit
