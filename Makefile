# Copyright (C) 2020 by Jonathan Appavoo, Boston University
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

CC=gcc
OBJCOPY=objcopy
LDS=A1.lds

.PHONY: q1 q2 q3 q1.resbin q2.resbin q3.resbin  part1

all: clean part1 

part1: q1 q2 q3

q1: q1.resbin
	echo HEXDUMP of q1.resbin
	hexdump q1.resbin
	echo Testing q1.resbin
	bash -c 'if md5sum q1.resbin | grep $$(cat q1.md5) ;\
        then echo q1: PASS; else echo q1: FAIL; fi'

q1.resbin: 
	gdb -x q1test.gdb

q2: q2.resbin
	echo HEXDUMP of q2.resbin
	hexdump q2.resbin
	echo Testing q2.resbin
	bash -c 'if md5sum q2.resbin | grep $$(cat q2.md5) ;\
	then echo q2: PASS; else echo q2: FAIL; fi'

q2.resbin:
	gdb -x q2test.gdb

q3: q3.resbin
	echo HEXDUMP of q3.resbin
	hexdump q3.resbin
	echo Testing q3.resbin
	bash -c 'if md5sum q3.resbin | grep $$(cat q3.md5) ;\
	then echo q3: PASS; else echo q3: FAIL; fi'

q3.resbin:
	gdb -x q3test.gdb


clean:
	-rm  *.lst *.map  *.o *.elf *.bin  *.resbin
