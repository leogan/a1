set pagination off
source kex.gdb

echo LOADING: q1.gdb\n
source q1.gdb

killKex

setup
startKex
continue

echo RUNNING: q1 from q1.gdb\n
q1

echo SAVING: gprs\n

savegprs q1.resbin
gprs

quit
