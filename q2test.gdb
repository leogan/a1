set pagination off
source kex.gdb

echo LOADING: q2.gdb\n
source q2.gdb

killKex

setup
startKex
continue

delete 1

echo RUNNING: q2 from q2.gdb\n
q2

echo TEST1: TESTING CODE AT 0x1000\n
set $rax = 0
set $rip = 0x1000
si
dump binary value q2.resbin { $rbx }


echo RUNNING: q2 from q2.gdb\n
q2

echo TEST2: TESTING CODE AT 0x1000\n
set $rax = -1 
set $rip = 0x1000
si
append binary value q2.resbin { $rbx }

echo RUNNING: q2 from q2.gdb\n
q2
echo TEST3: TESTING CODE AT 0x1000\n
set $rax = 0xaaaaaaaaaaaaaaaa
set $rip = 0x1000
si
append binary value q2.resbin { $rbx }


quit
